export function findFirstValueInArray(arr: any, value: any) {
  const matches = arr.filter((val: any) => {
    if (val == value) {
      return true;
    }

    return false;
  });

  return matches.length > 0 ? matches[0] : null;
}
