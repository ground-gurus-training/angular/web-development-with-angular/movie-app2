import { Injectable } from '@angular/core';
import { Auth, GoogleAuthProvider, signOut } from '@angular/fire/auth';
import { signInWithPopup } from '@firebase/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private auth: Auth) {}

  loginViaGoogle() {
    const provider = new GoogleAuthProvider();
    return signInWithPopup(this.auth, provider);
  }

  logout() {
    return signOut(this.auth);
  }
}
