import { Injectable } from '@angular/core';
import {
  collection,
  collectionData,
  doc,
  Firestore,
  getDocs,
  query,
  where,
} from '@angular/fire/firestore';
import { findFirstValueInArray } from '../utils/array-util.service';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  constructor(private firestore: Firestore) {}

  getFavorites() {
    const favoritesCol = collection(this.firestore, 'Favorites');
    return collectionData(favoritesCol);
  }

  async toggleFavorite(id: string) {
    const favorite = await this.getFavoritesByUserId(id);
    favorite.forEach((result) => {
      if (result) {
        const data: any = result.data();
        const movies = data.movies;
        // TODO find the movie by id
        // TODO if found then remove it and update the doc
        // TODO else add the movie id and update the doc
        // findFirstValueInArray(movies, id);
        // https://firebase.google.com/docs/firestore/manage-data/add-data?authuser=0#update-data
      }
    });
  }

  getFavoritesByUserId(userId: string) {
    const q = query(
      collection(this.firestore, 'Favorites'),
      where('userId', '==', userId)
    );
    return getDocs(q);
  }
}
