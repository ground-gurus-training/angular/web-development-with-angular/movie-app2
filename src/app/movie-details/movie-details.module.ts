import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieDetailsComponent } from './movie-details.component';

import { ChipModule } from 'primeng/chip';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [MovieDetailsComponent],
  imports: [ChipModule, ButtonModule, CommonModule],
  exports: [MovieDetailsComponent],
})
export class MovieDetailsModule {}
