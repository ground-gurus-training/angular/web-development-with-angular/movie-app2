import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Movie } from '../model/movie';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  movie: Movie;

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService,
    private router: Router
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id')!;

    if (id) {
      this.movieService.getMovie(id).subscribe((data: any) => {
        this.movie = {
          id: data.id,
          title: data.title,
          overview: data.overview,
          posterPath: data.poster_path,
          releaseDate: data.release_date,
        };

        if (data.genres) {
          this.movie.genres = [];
          data.genres.forEach((genre) => {
            this.movie.genres.push(genre.name);
          });
        }
      });
    }
  }

  goToHome() {
    this.router.navigateByUrl('/');
  }
}
