import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    ButtonModule,
    InputTextModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
