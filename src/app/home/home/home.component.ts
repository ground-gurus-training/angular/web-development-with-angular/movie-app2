import { Component, OnInit } from '@angular/core';
import { Auth, onAuthStateChanged } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Movie } from 'src/app/model/movie';
import { AuthService } from 'src/app/services/auth.service';
import { FavoritesService } from 'src/app/services/favorites.service';
import { MovieService } from 'src/app/services/movie.service';
import { findFirstValueInArray } from 'src/app/utils/array-util.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoggedIn = false;
  displayName;
  userId;
  movies: Movie[] = [];
  favoriteMovies: any[] = [];
  query = '';

  constructor(
    private auth: Auth,
    private authService: AuthService,
    private movieService: MovieService,
    private favoriteService: FavoritesService,
    private router: Router
  ) {}

  ngOnInit() {
    onAuthStateChanged(this.auth, (user) => {
      if (user) {
        this.isLoggedIn = true;
        this.displayName = user.displayName;
        this.userId = user.uid;
      } else {
        this.isLoggedIn = false;
        console.log('User is logged out!');
      }
    });
  }

  isMovieAFavorite(id: string) {
    return findFirstValueInArray(this.favoriteMovies, id) ? true : false;
  }

  findMovie(id: string) {
    return findFirstValueInArray(this.favoriteMovies, id);
  }

  async logout() {
    await this.authService.logout();
  }

  goToMovieDetails(id: string) {
    this.router.navigateByUrl(`/movie-details/${id}`);
  }

  goToLoginPage() {
    this.router.navigateByUrl('/login');
  }

  toggleFavorite(id: string) {
    const movie = this.findMovie(id);

    if (movie) {
      movie.isFavorite = !movie.isFavorite;
      this.favoriteService.toggleFavorite(id);
    }
  }

  async search() {
    // guard
    if (!this.query) return;

    const favoritesSnapshot = await this.favoriteService.getFavoritesByUserId(
      this.userId
    );
    favoritesSnapshot.forEach((doc) => {
      const data: any = doc.data();
      this.favoriteMovies = data.movies;
    });

    this.movies = [];
    this.movieService.search(this.query).subscribe((data: any) => {
      const results = data.results;
      results.forEach((m: any) => {
        this.movies.push({
          id: m.id,
          title: m.title,
          overview: m.overview,
          posterPath: m.poster_path,
          releaseDate: m.release_date,
          isFavorite: this.isMovieAFavorite(m.id),
        });
      });
    });
  }

  clear() {
    this.movies = [];
    this.query = '';
  }
}
