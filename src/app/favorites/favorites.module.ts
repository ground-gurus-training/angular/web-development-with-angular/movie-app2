import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';

import { TableModule } from 'primeng/table';

@NgModule({
  declarations: [FavoritesComponent],
  imports: [TableModule, CommonModule],
  exports: [FavoritesComponent],
})
export class FavoritesModule {}
